import { AppPage } from './app.po';

describe('angular-material-node-firebase-heroku App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should redirect to login page is user is not yet login', () => {
    page.navigateTo();

    page.expectCurrentUrl('/login?returnUrl=%2F');
  });

  describe('login page', () => {
    it('should redirect to homepage after login', () => {
      page.login();

      page.expectCurrentUrl('/');

      page.logout();
    });

    it('should show error in login when email is empty', () => {
      page.login('', 'e2e', 3000);

      page.expectCurrentUrl('/login');
      expect(page.getText('mat-error')).toBe('You must enter your email.');
    });

    it('should show error in login when email is invalid', () => {
      page.login('e2e.user', 'e2e', 3000);

      page.expectCurrentUrl('/login');
      expect(page.getText('mat-error')).toBe('Invalid email.');
    });

    it('should show error in login when email or password is invalid', () => {
      page.login('e2e.user@email.com', 'e2e', 3000);

      page.expectCurrentUrl('/login');
      expect(page.getText('mat-error')).toBe('The email or password is invalid.');
    });
  });

  describe('home page', () => {
    it('should logout', () => {
      page.login();
      page.expectCurrentUrl('/');

      page.logout();
      page.expectCurrentUrl('/login');
    });
  });
});
