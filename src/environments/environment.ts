// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  appTitle: 'Test Project',
  production: false,
  firebase: {
    apiKey: 'AIzaSyChUPigcwB28hFOX_LwTfy6LzT2NYBL1ro',
    authDomain: 'amnfh-4ec1b.firebaseapp.com',
    databaseURL: 'https://amnfh-4ec1b.firebaseio.com',
    projectId: 'amnfh-4ec1b',
    storageBucket: '',
    messagingSenderId: '331978808402'
  }
};
