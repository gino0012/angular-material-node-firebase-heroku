import { TestBed, async, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './common/services/auth/auth.service';
import { Router } from '@angular/router';
import { MockAuthService } from './common/test/mocks/mock-auth-service';
import { MockRouter } from './common/test/mocks/mock-router';
import { Observable } from 'rxjs/Observable';

describe('AppComponent', () => {
  let mockRouter, mockAuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        { provide: Router, useClass: MockRouter }
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));

  beforeEach(inject([Router, AuthService], (_mockRouter_, _mockAuthService_) => {
    mockRouter = _mockRouter_;
    mockAuthService = _mockAuthService_;
  }));

  it('should not navigate to return url if invalid user', async(() => {
    mockAuthService.user$ = Observable.of(null);

    TestBed.createComponent(AppComponent);

    expect(mockRouter.navigateByUrl).not.toHaveBeenCalled();
  }));

  it('should navigate to return url if valid user', async(() => {
    const MOCK_RETURN_URL = 'some/return/url';
    mockAuthService.user$ = Observable.of({name: 'test name'});
    localStorage.setItem('returnUrl', MOCK_RETURN_URL);

    TestBed.createComponent(AppComponent);

    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith(MOCK_RETURN_URL);
  }));
});
