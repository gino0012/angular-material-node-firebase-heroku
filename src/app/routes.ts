import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './common/services/auth-guard/auth-guard.service';
import { LoginComponent } from './login/login.component';

export const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent , canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent }
];
