import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { NavigationBarComponent } from './navigation-bar.component';
import { MatToolbarModule } from '@angular/material';
import { AuthService } from '../../common/services/auth/auth.service';
import { MockRouter } from '../../common/test/mocks/mock-router';
import { MockAuthService } from '../../common/test/mocks/mock-auth-service';
import { Router } from '@angular/router';
import { AuthInterface } from '../../common/services/auth/auth-interface';

describe('NavigationBarComponent', () => {
  let component: NavigationBarComponent;
  let fixture: ComponentFixture<NavigationBarComponent>;
  let mockRouter: MockRouter;
  let mockAuthService: AuthInterface;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationBarComponent ],
      imports: [ MatToolbarModule ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        { provide: Router, useClass: MockRouter }
      ]
    })
    .compileComponents();
  }));

  beforeEach(inject([AuthService, Router], (_mockAuthService_, _mockRouter_) => {
    mockAuthService = _mockAuthService_;
    mockRouter = _mockRouter_;

    fixture = TestBed.createComponent(NavigationBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should logout', () => {
    component.logout();

    expect(mockAuthService.logout).toHaveBeenCalled();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
  });
});
