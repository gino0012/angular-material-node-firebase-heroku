import { MainModule } from './main/main.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { MatComponentsModule } from '../mat-components.module';

@NgModule({
  imports: [
    CommonModule,
    MatComponentsModule,
    MainModule
  ],
  declarations: [
    HomeComponent,
    NavigationBarComponent
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
