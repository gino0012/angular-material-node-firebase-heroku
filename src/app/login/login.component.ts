import { Component } from '@angular/core';
import { AuthService } from '../common/services/auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loggingIn = false;
  hide = true;
  form: FormGroup;

  constructor(fb: FormBuilder, private auth: AuthService) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: []
    });
  }

  loginGoogle() {
    this.auth.loginGoogle();
  }

  loginEmail() {
    this.loggingIn = true;

    this.auth.loginEmail(this.form.value).then(() => {
      this.loggingIn = false;
    }, (err) => {
      this.form.setErrors({
        invalidLogin: {
          message: 'The email or password is invalid.'
        }
      });

      if (err.code === 'auth/network-request-failed') {
        this.form.setErrors({
          invalidLogin: {
            message: 'Error occurred while logging in. Please try again later.'
          }
        });
      }
      this.loggingIn = false;
    });
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }
}
