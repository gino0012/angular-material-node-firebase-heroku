import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthService } from '../common/services/auth/auth.service';
import { MockAuthService } from '../common/test/mocks/mock-auth-service';
import { ActivatedRoute } from '@angular/router';
import { MockActivatedRoute } from '../common/test/mocks/mock-activated-route';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LoginComponent', () => {
  let mockAuthService: MockAuthService;
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      declarations: [ LoginComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        FormBuilder,
        { provide: AuthService, useClass: MockAuthService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(inject([AuthService], (_mockAuthService_) => {
    mockAuthService = _mockAuthService_;

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should login', () => {
    component.loginGoogle();

    expect(mockAuthService.loginGoogle).toHaveBeenCalled();
  });

  describe('loginEmail', () => {
    const MOCK_CREDENTIALS = {
      email: 'mock@email.com',
      password: 'mockPassword'
    };

    beforeEach(() => {
      component.email.setValue(MOCK_CREDENTIALS.email);
      component.password.setValue(MOCK_CREDENTIALS.password);
    });

    it('should login with email', fakeAsync(() => {
      mockAuthService.loginEmail.and.returnValue(Promise.resolve());

      component.loginEmail();

      expectLoginEmail();
    }));

    it('should not login with email when invalid credentials', fakeAsync(() => {
      mockAuthService.loginEmail.and.returnValue(Promise.reject({code: ''}));

      component.loginEmail();

      expectLoginEmail();
      expect(component.form.errors.invalidLogin.message).toBe('The email or password is invalid.');
    }));

    it('should not login with email when error occured', fakeAsync(() => {
      mockAuthService.loginEmail.and.returnValue(Promise.reject({code: 'auth/network-request-failed'}));

      component.loginEmail();

      expectLoginEmail();
      expect(component.form.errors.invalidLogin.message).toBe('Error occurred while logging in. Please try again later.');
    }));

    function expectLoginEmail() {
      expect(mockAuthService.loginEmail).toHaveBeenCalledWith(MOCK_CREDENTIALS);
      expect(component.loggingIn).toBeTruthy();
      tick();
      expect(component.loggingIn).toBeFalsy();
    }
  });
});
