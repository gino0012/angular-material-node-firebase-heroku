import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { MockAngularFireAuth } from '../../test/mocks/mock-angular-fire-auth';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { MockActivatedRoute } from '../../test/mocks/mock-activated-route';

describe('AuthService', () => {
  const USER = {name: 'test name'};

  let mockAngularFireAuth: MockAngularFireAuth;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: AngularFireAuth, useClass: MockAngularFireAuth },
        { provide: ActivatedRoute, useClass: MockActivatedRoute }
      ]
    });
  });

  beforeEach(inject([AngularFireAuth], (_mockAngularFireAuth_) => {
    mockAngularFireAuth = _mockAngularFireAuth_;
    mockAngularFireAuth.authState = Observable.of(USER);
  }));

  it('should have a correct user', inject([AuthService], (service: AuthService) => {
    let userSpy = jasmine.createSpy('user');

    service.user$.subscribe(userSpy);

    expect(userSpy).toHaveBeenCalledWith(USER);
  }));

  it('should login', inject([AuthService], (service: AuthService) => {
    service.loginGoogle();

    expect(mockAngularFireAuth.auth.signInWithRedirect).toHaveBeenCalled();
  }));

  it('should login with email and password', inject([AuthService], (service: AuthService) => {
    const MOCK_CREDENTIALS = {
      email: 'email@y.com',
      password: 'p@ssw0rd123'
    };

    service.loginEmail(MOCK_CREDENTIALS);

    expect(mockAngularFireAuth.auth.signInWithEmailAndPassword).toHaveBeenCalledWith(MOCK_CREDENTIALS.email, MOCK_CREDENTIALS.password);
  }));

  it('should logout', inject([AuthService], (service: AuthService) => {
    service.logout();

    expect(mockAngularFireAuth.auth.signOut).toHaveBeenCalled();
  }));
});
