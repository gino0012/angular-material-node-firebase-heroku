export interface AuthInterface {
  loginGoogle(): void;
  logout(): void;
  loginEmail(): Promise<any>;
}
