export class MockRouter {
  navigateByUrl = jasmine.createSpy('router navigateByUrl');
  navigate = jasmine.createSpy('router navigate');
}
