import { AuthInterface } from '../../services/auth/auth-interface';
import { Observable } from 'rxjs/Observable';

export class MockAuthService implements AuthInterface {
  user$: Observable<{}>;

  loginGoogle = jasmine.createSpy('auth login');
  logout = jasmine.createSpy('auth logout');
  loginEmail = jasmine.createSpy('auth loginEmail');
}
