# Angular Material Node Firebase Heroku

[![pipeline status](https://gitlab.com/gino0012/angular-material-node-firebase-heroku/badges/master/pipeline.svg)](https://gitlab.com/gino0012/angular-material-node-firebase-heroku/commits/master) [![coverage report](https://gitlab.com/gino0012/angular-material-node-firebase-heroku/badges/master/coverage.svg)](https://gitlab.com/gino0012/angular-material-node-firebase-heroku/commits/master)

Deployed: https://amnfh.herokuapp.com/

## Setup

Install [node](https://nodejs.org/en/)

Run in cmd or terminal:

    npm install -g @angular/cli nodemon

## For Developer

Run `npm run develop` for a dev server. Navigate to `http://localhost:4200/`.

## For User

Run `npm run serve` for user. Navigate to `http://localhost:4200/`.

## Running unit tests

Run `npm test` to execute the unit tests

## Running end-to-end tests

Run `npm run serve` and run `npm run e2e` in another terminal.
