#!/bin/bash

PROD="$(printenv PROD)"

if [ "$PROD" == "true" ]
then
    echo "building production"
	ng build --prod
else
    echo "building develop"
	ng build
fi